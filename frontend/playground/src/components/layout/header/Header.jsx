import React from 'react'
import "./header.css"
import logoChico  from "../../../resources/media/logo_chico.svg"

const Header = () => {
  return (
    <header>
      <div className='logo'>
        <img src={logoChico} alt="logo eduapp" />
      </div>
      <div className='menu'>menu</div>
    </header>
  )
}

export default Header