package com.playground.apistudent.exceptions;

public enum MessageError {

    STUDENT_DOES_NOT_EXIST("El estudiante solicitado no existe");

    String message;

    MessageError(String message) {
        this.message = message;
    }
}
