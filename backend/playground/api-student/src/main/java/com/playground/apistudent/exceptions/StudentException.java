package com.playground.apistudent.exceptions;


public class StudentException extends Exception{

    public StudentException(MessageError message) {
        super(message.message);
    }
}
