package com.playground.apistudent.controller;

import com.playground.apistudent.exceptions.StudentException;
import com.playground.apistudent.model.Student;
import com.playground.apistudent.service.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/students")
public class StudentController {

    @Autowired
    private IStudentService studentService;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public ResponseEntity<Long> create(@RequestBody Student student) {
        studentService.create(student);
        return ResponseEntity.ok(student.getStudentId());
    }

    @GetMapping("/{id}")
    public Student getById(@PathVariable Long id) throws StudentException {
        return studentService.getById(id);
    }

    @GetMapping
    public List<Student> getAll() {
        return studentService.getAll();
    }

    @DeleteMapping("/{id}")
    public Long deleteById(@PathVariable Long id) {
        return studentService.deleteById(id);
    }
}
